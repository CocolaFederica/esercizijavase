package EsercitazioneOmicron;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class GaraFactory {

	public static Gara creaGara(GaraEnum tipoGara, List<Atleta> elencoPartecipanti, GregorianCalendar dataGara) {

		if (tipoGara.equals(GaraEnum.MARATONA)) {
			List<Maratoneta> maratoneti = new ArrayList<Maratoneta>();
			for (Atleta atleta : elencoPartecipanti) {
				if (atleta instanceof Maratoneta) {
					maratoneti.add((Maratoneta) atleta);
				}

			}
			return new GaraMaratona(tipoGara, maratoneti, dataGara);
		}
		
		if (tipoGara.equals(GaraEnum.LANCIO_DEL_DISCO)) {
			List<LanciatoreDelDisco> lanciatori = new ArrayList<LanciatoreDelDisco>();
			for (Atleta atleta : elencoPartecipanti) {
				if (atleta instanceof LanciatoreDelDisco) {
					lanciatori.add((LanciatoreDelDisco) atleta);
				}

			}
			return new GaraLancio(tipoGara, lanciatori, dataGara);
			
		}

		return new Gara(null, elencoPartecipanti, dataGara, "");
	}


}