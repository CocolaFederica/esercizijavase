package EsercitazioneOmicron;

public class Main {

	private static final String NazDiscoDue = null;

	public static void main(String[] args) {

		System.out.println("La prima categoria in gara in queste Olimpiadi 2019 �: \"Lanciatori del Disco\"!");
		LanciatoreDelDisco atlL1 = new LanciatoreDelDisco();
		LanciatoreDelDisco atlL2 = new LanciatoreDelDisco();

		atlL1.setNazionalita(Nazionalita.FRANCIA);
		atlL1.setNome("James");
		atlL1.setCognome("Sutton");
		atlL1.setId(123);
		atlL1.scriviOutput();
		System.out.println();

		System.out.println("Vi presentiamo il secondo in gara!");
		System.out.println();

		atlL2.setNazionalita(Nazionalita.SPAGNA);
		atlL2.setNome("Luis");
		atlL2.setCognome("Pilot");
		atlL2.setId(225);
		atlL2.scriviOutput();
		System.out.println();

//		LanciatoreDelDisco vincitoreLanciatoriDisco = new LanciatoreDelDisco();
//		vincitoreLanciatoriDisco.setClassifica(2);
//		vincitoreLanciatoriDisco.getClassifica();

		System.out.println("La seconda categoria in gara in queste Olimpiadi 2019 �: \"Maratoneti\"!");
		Maratoneta atlM1 = new Maratoneta();
		Maratoneta atlM2 = new Maratoneta();
		Maratoneta atlM3 = new Maratoneta();
		Maratoneta atlM4 = new Maratoneta();

		atlM1.setNazionalita(Nazionalita.ITALIA);
		atlM1.setNome("Jessica");
		atlM1.setCognome("Low");
		atlM1.setId(222);
		atlM1.scriviOutput();
		System.out.println();

		System.out.println("Vi presentiamo il secondo, terzo e quarto in gara!");

		atlM2.setNazionalita(Nazionalita.RUSSIA);
		atlM2.setNome("Natalia");
		atlM2.setCognome("Zgherea");
		atlM2.setId(362);
		atlM2.scriviOutput();
		System.out.println();

		atlM3.setNazionalita(Nazionalita.AMERICA);
		atlM3.setNome("Alice");
		atlM3.setCognome("Swift");
		atlM3.setId(587);
		atlM3.scriviOutput();
		System.out.println();

		atlM4.setNome("Homer");
		atlM4.setCognome("Simpson");
		atlM4.setId(124);
		atlM4.setNazionalita(Nazionalita.FRANCIA);
		atlM4.scriviOutput();
		System.out.println();

		Maratoneta vincitoreMaratoneta = new Maratoneta();
		vincitoreMaratoneta.setClassifica("Natalia Zgherea", "Alice Swift", "Homer Simpson", "Jessica Low");
//		vincitoreMaratoneta.getClassifica();

	}
}

/*
 * LanciatoreDelDisco vincitoreLanciatoriDisco = new LanciatoreDelDisco();
 * vincitoreLanciatoriDisco.setClassifica("Thomas Pilot","James Sutton");
 * vincitoreLanciatoriDisco.getClassifica(); }
 */
