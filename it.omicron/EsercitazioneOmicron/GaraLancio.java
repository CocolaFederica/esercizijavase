package EsercitazioneOmicron;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class GaraLancio extends Gara {
	List<LanciatoreDelDisco> lanciatoriDelDisco;

	public GaraLancio(GaraEnum lancio, List<LanciatoreDelDisco> lanciatoriDelDisco, GregorianCalendar dataLancio) {
		super(lancio, dataLancio, "");
		this.lanciatoriDelDisco = lanciatoriDelDisco;
	}

	@Override
	public String getDescrizione() {
		return "QUESTA E' UNA GARA LANCIO DEL DISCO!";

	}

	public List<LanciatoreDelDisco> getElencoPartecipanti() {
		return lanciatoriDelDisco;
	}
	
//	public void dataGaraLancio() {
//		Calendar cal = Calendar.getInstance();
//		cal.set(2020, 01, 30);
//		System.out.println(cal.getTime());
//	}


}
