package EsercitazioneOmicron;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class GaraMaratona extends Gara {
	List<Maratoneta> maratoneti;

	public GaraMaratona(GaraEnum maratona, List<Maratoneta> maratoneti, GregorianCalendar dataMaratona) {
		super(maratona, dataMaratona, "");
		this.maratoneti = maratoneti;
	}

	@Override
	public String getDescrizione() {
		return "QUESTA E' UNA GARA MARATONA!";
	}

	public List<Maratoneta> getElencoPartecipanti() {
		return maratoneti;
	}

//	public void setDataGara() {
//		Calendar cal = Calendar.getInstance();
//		cal.set(2020, 01, 25);
//		System.out.println(cal.getTime());
//	}
}
