package EsercitazioneOmicron;

public class Atleta {
	private String nome;
	private String cognome;
	private int id;
	private Nazionalita nazionalita;;

	public Atleta() {
		super();
	}

	public Atleta(String nome, String cognome, int id, Nazionalita nazionalita) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.id = id;
		this.nazionalita = nazionalita;
	}

	public Atleta(String nomeAtl, String cognomeAtl, int idAtl) {
		super();
		this.nome = nomeAtl;
		this.cognome = cognomeAtl;
		this.id = idAtl;
	}

	public void setNome(String nomeAtl) {
		if (nomeAtl != null && nomeAtl != "")
			nome = nomeAtl;
		else {
			System.out.println("Errore: nome errato!");
			System.exit(0);
		}
	}

	public Nazionalita getNazionalita() {
		return nazionalita;
	}

	public void setNazionalita(Nazionalita nazionalita) {
		this.nazionalita = nazionalita;
	}

	public void setCognome(String cognomeAtl) {
		if (cognomeAtl != null && cognomeAtl != "")
			cognome = cognomeAtl;
		else {
			System.out.println("Errore : cognome errato!");
			System.exit(0);
		}
	}

	public void setId(int idAtl) {
		if (idAtl != 0) {
			id = idAtl;
		} else {
			System.out.println("Errore : id deve essere diverso da 0!");
			System.exit(0);
		}

	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Atleta{" + "nome:" + nome + ", cognome:" + cognome + ", id:" + id + ", nazionalita: " + nazionalita
				+ '}';
	}

}
