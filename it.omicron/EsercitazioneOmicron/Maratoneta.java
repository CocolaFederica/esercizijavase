package EsercitazioneOmicron;

public class Maratoneta extends Atleta {
	private String primoClassificato;
	private String secondoClassificato;
	private String terzoClassificato;
	private String quartoClassificato;

	public Maratoneta() {
		super();
	}

	public Maratoneta(String nome, String cognome, int id, Nazionalita nazionalita) {
		super(nome, cognome, id, nazionalita);
	}

	public Maratoneta(int classifica, String primoClassificato, String secondoClassificato, String terzoClassificato,
			String quartoClassificato) {
		super();
		this.primoClassificato = primoClassificato;
		this.secondoClassificato = secondoClassificato;
		this.terzoClassificato = terzoClassificato;
		this.quartoClassificato = quartoClassificato;
	}

	public String getPrimoClassificato() {
		return primoClassificato;
	}

	public void setPrimoClassificato(String primoClassificato) {
		this.primoClassificato = primoClassificato;
	}

	public String getSecondoClassificato() {
		return secondoClassificato;
	}

	public void setSecondoClassificato(String secondoClassificato) {
		this.secondoClassificato = secondoClassificato;
	}

	public String getTerzoClassificato() {
		return terzoClassificato;
	}

	public void setTerzoClassificato(String terzoClassificato) {
		this.terzoClassificato = terzoClassificato;
	}

	public String getQuartoClassificato() {
		return quartoClassificato;
	}

	public void setQuartoClassificato(String quartoClassificato) {
		this.quartoClassificato = quartoClassificato;
	}

	public void scriviOutput() {
		System.out.println("Nome: " + getNome());
		System.out.println("Cognome: " + getCognome());
		System.out.println("Id: " + getId());
		System.out.println("La nazionalit� �: " + getNazionalita());
	}

	public void setClassifica(String primoAtl, String secondoAtl, String terzoAtl, String quartoAtl) {
		this.primoClassificato = primoAtl;
		this.secondoClassificato = secondoAtl;
		this.terzoClassificato = terzoAtl;
		this.quartoClassificato = quartoAtl;
		System.out.println("IL VINCITORE E': " + primoAtl);
		System.out.println();
	}

	public void secondoClass(String secondo) {
		secondo = secondoClassificato;
		System.out.println("Il secondo classificato �:" + secondo);
	}

	public void terzoClass(String terzo) {
		terzo = terzoClassificato;
		System.out.println("Il secondo classificato �:" + terzo);
	}
}
