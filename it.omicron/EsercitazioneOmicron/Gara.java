package EsercitazioneOmicron;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class Gara extends Atleta {
	private GaraEnum gara;
	private List<Atleta> partecipanti;
	private GregorianCalendar dataGara;
	private String descrizione = "SONO UNA GARA GENERICA";

	public Gara(GaraEnum gara, List<Atleta> partecipanti, GregorianCalendar dataLancio, String descrizione) {
		super();
		this.gara = gara;
		this.partecipanti = partecipanti;
		this.dataGara = dataLancio;
		this.descrizione = descrizione;
	}

	public Gara(GaraEnum maratona, GregorianCalendar dataMaratona, String descrizione) {
		super();
		this.gara = gara;
		this.dataGara = dataGara;
		this.descrizione = descrizione;
	}

	public void setTipoGara(String lancioDisco, String maratona) {
	}

	public GaraEnum getGara() {
		return gara;
	}

	public void setGara(GaraEnum gara) {
		this.gara = gara;
	}

	public List<Atleta> getPartecipanti() {
		return this.partecipanti;
	}

	public Calendar getDataGara() {
		return this.dataGara;
	}

//	public String toStringData() {
//		return "Data Gara" + dataGara;
//	}

	@Override
	public String toString() {
		return "Gara{" + "tipoGara:" + gara + ", partecipanti:" + partecipanti + ", dataGara:" + dataGara + '}';
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

}
