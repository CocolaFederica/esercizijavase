
public class Casa {

	private String tetto;
	private String fondamenta;
	private String muri;

	public Casa() {
		super();
	}

	public Casa(String tetto, String fondamenta, String muri) {
		super();
		this.tetto = tetto;
		this.fondamenta = fondamenta;
		this.muri = muri;
	}

	public String getTetto() {
		return tetto;
	}

	public void setTetto(String tetto) {
		this.tetto = tetto;
	}

	public String getFondamenta() {
		return fondamenta;
	}

	public void setFondamenta(String fondamenta) {
		this.fondamenta = fondamenta;
	}

	public String getMuri() {
		return muri;
	}

	public void setMuri(String muri) {
		this.muri = muri;
	}

}
