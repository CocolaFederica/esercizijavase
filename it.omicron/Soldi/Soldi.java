package Soldi;

import java.util.Scanner;

public class Soldi {
	
	private long euro;
	private long centesimi;
	
	public void set(long nuoviEuro) {
		if (nuoviEuro < 0) {
			System.out.println("Errore: somme negative di soldi" + " non sono permesse.");
			System.exit(0);
		} else {
				euro = nuoviEuro;
				centesimi = 0;
		}
	}
	
	public void set(double nuovaSomma) {
		if (nuovaSomma < 0) {
			System.out.println("Errore: somme negative di soldi" + " non sono permesse.");
			System.exit(0);
		} else {
			long centesimiTotali = Math.round(nuovaSomma * 100);
			euro = centesimiTotali / 100;
			centesimi = centesimiTotali % 100;
		}
	}
	
	public void set(Soldi altriSoldi) {
		this.euro = altriSoldi.euro;
		this.centesimi = altriSoldi.centesimi;
	}
	
	public void set(String sommaString) {
		String stringaEuro;
		String stringaCentesimi;
		// cancella la 'E' iniziale se presente
		if (sommaString.charAt(0) == 'E') 
			sommaString = sommaString.substring(1);
			sommaString = sommaString.trim(); 
			// il metodo trim della classe String non accetta parametri e in pratica toglie gli spazi nella stringa
			sommaString = sommaString.substring(1);
			sommaString = sommaString.trim();
			
			int posizionePunto = sommaString.indexOf(".");
			if (posizionePunto < 0) {
				centesimi = 0;
				euro = Long.parseLong(sommaString);
			} else {
				stringaEuro = sommaString.substring(0, posizionePunto);
				stringaCentesimi = sommaString.substring(posizionePunto + 1);
				
				if (stringaCentesimi.length() <= 1)
					stringaCentesimi = stringaCentesimi + "0";
				euro = Long.parseLong(stringaEuro);
				centesimi = Long.parseLong(stringaCentesimi);
				if ((euro < 0) || (centesimi < 0) || (centesimi > 99)) {
					System.out.println("Errore: rappresentazione illegale di soldi");
					System.exit(0);
				}
				
			}
			
		}
		
		public void leggiInput() {
			System.out.println("Scrivi l'ammontare su una riga:");
			Scanner tastiera = new Scanner(System.in);
			String somma = tastiera.nextLine();
			set(somma.trim());
			
			
		}
		
		public void scriviOutput() {
			System.out.println("E " + euro);
			if (centesimi < 10)
				System.out.println(".0" + centesimi);
			else 
				System.out.println("." + centesimi);
		}
		
		public Soldi moltiplica(int n) {
			Soldi prodotto = new Soldi();
			prodotto.centesimi = n * centesimi;
			long riporto = prodotto.centesimi / 100;
			prodotto.centesimi = prodotto.centesimi % 100;
			return prodotto;
		}
		
		public Soldi somma(Soldi altriSoldi) {
			Soldi somma = new Soldi();
			somma.centesimi = this.centesimi + altriSoldi.centesimi;
			long riporto = somma.centesimi / 100;
			somma.centesimi = somma.centesimi % 100;
			somma.euro = this.euro + altriSoldi.euro + riporto;
			return somma;
			
		}
			
	}


