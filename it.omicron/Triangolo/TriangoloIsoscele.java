package Triangolo;

public class TriangoloIsoscele implements TriangoloInterface {
	private String nomeTriangolo;
	private int base;
	private int altezza;

	public TriangoloIsoscele() {
		super();
	}

	public TriangoloIsoscele(String nomeTriangolo, int base, int altezza) {
		super();
		this.nomeTriangolo = nomeTriangolo;
		this.base = base;
		this.altezza = altezza;
	}

	public String getNomeTriangolo() {
		return nomeTriangolo;
	}

	public void setNomeTriangolo(String nomeTriangolo) {
		this.nomeTriangolo = nomeTriangolo;
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getAltezza() {
		return altezza;
	}

	public void setAltezza(int altezza) {
		this.altezza = altezza;
	}

	@Override
	public int getArea() {
		return (base * altezza) / 2;
	}

}
