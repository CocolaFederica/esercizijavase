package Triangolo;

public class TriangoloFactory {

	public static TriangoloInterface createTriangolo(String tipoTriangolo, int base, int altezza) {
		if ("isoscele".equalsIgnoreCase(tipoTriangolo)) { // sempre meglio mettere prima la stringa e dopo il parametro
			// i questo modo la stringa non punta a "null"
			return new TriangoloIsoscele(tipoTriangolo, base, altezza);
		}

		if ("equilatero".equalsIgnoreCase(tipoTriangolo)) {
			return new TriangoloEquilatero(tipoTriangolo, base, altezza);
		}

		if ("scaleno".equalsIgnoreCase(tipoTriangolo)) {
			return new TriangoloEquilatero(tipoTriangolo, base, altezza);
		}

		return null;
	}
}
