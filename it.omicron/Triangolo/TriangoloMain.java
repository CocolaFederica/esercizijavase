package Triangolo;

import java.util.Scanner;

public class TriangoloMain {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Inserisci tre numeri non negativi");
		int lato1 = input.nextInt();
		int base = input.nextInt();
		int lato2 = input.nextInt();

		TriangoloInterface triangolo = null;

		if (lato1 == lato2 && (base > lato1 || base < lato1)) {
			System.out.println("Il triangolo � ISOSCELE.");
			triangolo = TriangoloFactory.createTriangolo("isoscele", 3, 2); // FACTORY TRIANGOLO

		} else if (lato1 == lato2 && lato1 == base && base == lato2) {
			System.out.println("Il triangolo non � ISOSCELE, ma EQUILATERO");
			triangolo = TriangoloFactory.createTriangolo("equilatero", 5, 2); // FACTORY TRIANGOLO

		} else if (lato1 != lato2 && lato1 != base && base != lato2) {
			System.out.println("Il triangolo non � ISOSCELE, ne EQUILATERO ma SCALENO"); // FACTORY TRIANGOLO
			triangolo = TriangoloFactory.createTriangolo("scaleno", 5, 2);

		} else if (lato1 == base) {
			System.out.println("NO TRIANGOLI!");
		}
		System.out.println("L'area del triangolo �: " + triangolo.getArea() + "cm quadrati");

	}
}
