package Triangolo;

public class TriangoloEquilatero implements TriangoloInterface {
	private String nomeTriangolo;
	private int lato;
	private int altezza;

	public TriangoloEquilatero() {
		super();
	}

	public TriangoloEquilatero(String nomeTriangolo, int lato, int altezza) {
		super();
		this.nomeTriangolo = nomeTriangolo;
		this.lato = lato;
		this.altezza = altezza;
	}

	public void setNomeTriangolo() {
		this.nomeTriangolo = nomeTriangolo;
	}

	public String getNomeTriangolo() {
		return nomeTriangolo;
	}

	public void setLato() {
		this.lato = lato;
	}

	public int getLato() {
		return lato;
	}

	@Override
	public int getArea() {
		return (int) ((Math.sqrt(3) * lato * lato) / 2); // L'area del Triangolo equilatero �: radice di 3 * L al
															// quadrato tutto diviso 2
	}

}
