package SelectionSort;

public class SelectionSort {
	
	public static void selectionSort(int[] unArray) {
		for (int indice = 0; indice < unArray.length - 1; indice ++) {
			int indiceDelSuccessivoPi¨Piccolo =
					getIndiceDelPi¨Piccolo(indice, unArray);
			scambio(indice, indiceDelSuccessivoPi¨Piccolo, unArray);
		}
	}

	public static int getIndiceDelPi¨Piccolo (int indiceInizio, int[] a) {
		int minimo = a[indiceInizio];
		int indiceDelMinimo = indiceInizio;
		
		for (int indice = indiceInizio + 1; indice < a.length; indice ++ ) {
			if (a[indice] < minimo) {
				minimo = a[indice];
				indiceDelMinimo = indice;
			}
		}
		return indiceDelMinimo;
	}	
		
		public static void scambio(int i, int j, int[] a) {
			int temp = a[i];
			a[i] = a[j];
			a[j] = temp;
		}
}
	