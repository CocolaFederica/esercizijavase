package ContoAllaRovescia;

import java.util.Scanner;

public class ContoAllaRovescia {

	public static void main(String[] args) {
		int valore = leggiNumero();
		mostraContoAllaRovescia(valore);

	}

	public static int leggiNumero() {
		System.out.println("Inserire un intero positivo:");
		Scanner input = new Scanner(System.in);
		int conteggio = input.nextInt();
		if (conteggio <= 0) {
			System.out.println("Il dato deve essere positivo.");
			System.out.println("Riprovare.");
			conteggio = leggiNumero();
		}
		return conteggio;
	}

	public static void mostraContoAllaRovescia(int conteggio) {
		System.out.println("Conto alla rovescia:");
		for (int rimanenti = conteggio; rimanenti >= 0; rimanenti--)
			System.out.println(rimanenti + ", ");
		System.out.println("Decollo!");
	}

}
