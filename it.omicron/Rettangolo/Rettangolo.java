package Rettangolo;

public class Rettangolo {
	
	private int larghezza;
	private int altezza;
	private int area;
	
	public int getLarghezza() {
		return larghezza;
	}

	public void setLarghezza(int larghezza) {
		this.larghezza = larghezza;
	}

	public int getAltezza() {
		return altezza;
	}

	public void setAltezza(int altezza) {
		this.altezza = altezza;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}
	
	
	public Integer calcolaArea(int larghezza, int altezza) {
		return area = larghezza * altezza;
	}
	
	
}


