package Specie;

import java.util.Scanner;

public class Specie {
	
	private String nome;
	private int popolazione;
	private double tassoCrescita;
	
	public Specie() {
		super();
	}
	
	public Specie(String nome, int popolazione, double tassoCrescita) {
		super();
		this.nome = nome;
		this.popolazione = popolazione;
		this.tassoCrescita = tassoCrescita;
	}

	public void leggiInput() {
		Scanner tastiera = new Scanner(System.in);
		System.out.println("Quale � il nome della specie?");
		nome = tastiera.nextLine();
		System.out.println("A quanto ammonta la popolazione?");
		popolazione = tastiera.nextInt();
		System.out.println("Inserisci il tasso di crescita ");
		tassoCrescita = tastiera.nextDouble();
		
	}
	
	public void scriviOutput() {
		System.out.println("Nome = " + nome);
		System.out.println("Popolazione = " + popolazione);
		System.out.println("Tasso di crescita = " + tassoCrescita + "%");	
	}
	

	public int prediciPopolazione(int anni) {
		
		anni = Math.abs(anni);
		
		int risultato = 0;
		double totalePopolazione = popolazione;
		int contatore = anni;
		
		while ((contatore > 0) && (totalePopolazione > 0)) {
			totalePopolazione = (totalePopolazione +
					(tassoCrescita / 100) * totalePopolazione);
					contatore --;
					
		}
		if (totalePopolazione > 0) 
			risultato = (int)totalePopolazione;
		return risultato;
		
		
	}
	
	public void setSpecie(String nuovoNome, int nuovaPopolazione, double nuovoTassoCrescita) {
		nome = nuovoNome;
		if (nuovaPopolazione >= 0)
			popolazione = nuovaPopolazione;
		else {
			System.out.println("ERRORE: si sta usando un numero negativo " + "per la popolazine. ");
			System.exit(0);
			
		}
	
	tassoCrescita = nuovoTassoCrescita;
}

public String getNome() {
	return nome;
	
}

public int getPopolazione() {
	return popolazione;
	
}
	
/*
	public boolean equals(Specie altroOggetto ) {
		return (this.nome.equalsIgnoreCase(altroOggetto.nome))
				&& (this.popolazione == altroOggetto.popolazione)
				&& (this.tassoCrescita == altroOggetto.tassoCrescita);
				
	}
*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + popolazione;
		long temp;
		temp = Double.doubleToLongBits(tassoCrescita);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Specie other = (Specie) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (popolazione != other.popolazione)
			return false;
		if (Double.doubleToLongBits(tassoCrescita) != Double.doubleToLongBits(other.tassoCrescita))
			return false;
		return true;
	}

	
	
}

