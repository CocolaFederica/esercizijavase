package Specie;

public class SpecieEqualsTest {
	
	public static void main (String[] args) {
		
		Specie s1 = new Specie("Bufalo Klingon", 10, 15);
		Specie s2 = new Specie("Bufalo Klingon", 10, 15);
		//Specie s1 = null;
		//Specie s2 = null;
	
		/*
		s1.setSpecie("Bufalo Klingon", 10, 15);
		s2.setSpecie("Bufalo Klingon", 10, 15);
		*/
		
		//Specie.prediciPopolazione(10);
		
		
		int res = s1.prediciPopolazione(-5);
		
		if (s1 == s2)
			System.out.println("Corrispondono secondo ==.");
		else 
			System.out.println("Non corrispondono secondo ==.");
		
		if (s1.equals(s2))
			System.out.println("Corrispondono secondo il metodo equals.");
		else 
			System.out.println("Non corrispondono secondo il metodo equals.");
		
		System.out.println("Ora cambiamo un Klingon in lettere minuscole.");
		s2.setSpecie("bufalo klingon", 10, 15);
		
		if (s1.equals(s2))
			System.out.println("Corrispondono secondo il metodo equals.");
		else 
			System.out.println("Non corrispondono secondo il metodo equals.");
			
		
			
	}

}
