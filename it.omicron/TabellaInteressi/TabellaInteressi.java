package TabellaInteressi;

public class TabellaInteressi {

	public static final int RIGHE = 10;
	public static final int COLONNE = 6;

	public static void main(String[] args) {
		int[][] tabella = new int[RIGHE][COLONNE];

		for (int riga = 0; riga < RIGHE; riga++)
			for (int colonna = 0; colonna < COLONNE; colonna++)
				tabella[riga][colonna] = getBilancio(1000.0, riga + 1, (5 + 0.5 * colonna));

		System.out.println("Bilanci per vari tassi di interesse " + " Capitallizzazione annuale ");
		System.out.println();
		System.out.println("Anni 5.00% 5.50% 6.00% 6.50% 7.00% 7.50%");
		visualizzaTabella(tabella);
	}

	public static void visualizzaTabella(int[][] unArray) {

		for (int riga = 0; riga < RIGHE; riga++) {
			System.out.print((riga + 1) + "  ");

			for (int colonna = 0; colonna < COLONNE; colonna++) {
				System.out.print("�" + unArray[riga][colonna] + " ");

			System.out.println();
		}
	}

	}

	public static int getBilancio(double bilancioIniziale, int anni, double tasso) {

		double bilancioCorrente = bilancioIniziale;

		for (int conteggio = 1; conteggio <= anni; conteggio++)
			bilancioCorrente = bilancioCorrente * (1 + tasso / 100);

		return (int) (Math.round(bilancioCorrente));

	}

}
